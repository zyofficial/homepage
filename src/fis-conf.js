fis.match('::packager', {
  spriter: fis.plugin('csssprites')
});



fis.match('*', {
  useHash: true
});

fis.match('*.html', {
  useHash: false
});

fis.match('*.config', {
  useHash: false
});

fis.match('*.js', {
  optimizer: fis.plugin('uglify-js')
});

fis.match('*.css', {
  useSprite: true,
  optimizer: fis.plugin('clean-css')
});

fis.match('*.png', {
  optimizer: fis.plugin('png-compressor')
});

fis.match('*.{png,jpg,gif,jpeg}', {
  optimizer: fis.plugin('img-compressor')
});
