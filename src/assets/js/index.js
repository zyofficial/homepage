var hwljs = hwljs || {};
hwljs.imgScroll = function() {
	$li1 = $(".zhilu-box .zl-road li");
	$window1 = $(".zhilu-box .zl-road ul");
	$left1 = $(".zhilu-box .prev");
	$right1 = $(".zhilu-box .next");
	$window1.css("width", $li1.length*325);
	var lc1 = 0;
	var rc1 = $li1.length-4;
	
	$left1.click(function(){
		if (lc1 < 1) {
			return;
		}
		lc1--;
		rc1++;
		$window1.animate({left:'+=325px'}, 500);
	});
	$right1.click(function(){
		if (rc1 < 1){
			return;
		}
		lc1++;
		rc1--;
		$window1.animate({left:'-=325px'}, 500);
	});
	hwljs.dropdown = function() {
		$('.lec-dropdown').hover(function(){
			$(this).addClass('active');
			$(this).find('.dp-box').stop().fadeIn();
		},function(){
			$(this).removeClass('active');
			$(this).find('.dp-box').fadeOut();
		})
	};
};
$(function() {
	hwljs.imgScroll();
	hwljs.dropdown();
});

$(function(){
	//微信弹出
	$(".weibo").mouseover(function(){
		$(this).children('.wechatT').fadeIn(200);
	});
	$(".weibo").mouseleave(function(){
	  	$(this).children('.wechatT').fadeOut(100);
	});
	
	$( '.nav>li' ).hover(function(){
		$(this).find('.box').stop().slideDown(200);
	},function(){
		$(this).find('.box').slideUp(200);
	})
	
	//弹出层
	$(".apply, .lookAll").click(function(){
		$(".layuiBox ,.layuiBg, .wenhide").css({display:"block"});
	});
	
	$(".layuiBg ,.close, .Wclose").click(function(){
		$(".layuiBox ,.layuiBg, .wenhide").css({display:"none"});
	});
	
	//tab切换
	$(".menu-tab li").click(function(){
	    $(this).siblings().removeClass('active');
	    $(this).addClass('active');
	    var num = $(".menu-tab li").index(this);
	    $(".tab-con").hide();
	    $(".tab-con").eq(num).show();
	});
	$(".invTab-menu li").click(function(){
	    $(this).siblings().removeClass('active');
	    $(this).addClass('active');
	    var num = $(".invTab-menu li").index(this);
	    $(".invTab-box").hide();
	    $(".invTab-box").eq(num).show();
	});
	
})